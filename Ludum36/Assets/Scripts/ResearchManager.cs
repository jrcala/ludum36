﻿using UnityEngine;
using System.Collections;

public class ResearchManager : MonoBehaviour
{

    public int swordTier = 0;
    public int spearTier = 0;
    public int bowTier = 0;

    public int maxSwordTier = 0;
    public int maxSpearTier = 0;
    public int maxBowTier = 0;

    public SpawnManager.UnitCost[] researchCosts;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        GameController.Instance.researchManager = this;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public SpawnManager.ResourceCost GetResearchCost(UNIT_TYPES unitType)
    {
        int tier = GetTier(unitType);
        return researchCosts[(int)unitType].resourceCost[tier];
    }

    public string GetCostString(UNIT_TYPES unitType)
    {
        string ret = "--/--/--/--";

        int currTier = GetTier(unitType);
        int maxTier = GetMaxTier(unitType);

        if (currTier == maxTier &&
            maxTier <= 2)
        {
            ret = SpawnManager.ResourceCostToString(GetResearchCost(unitType));
        }

        return ret;
    }

    public void DecreaseResearchLevel(int unitType)
    {
        DecreaseResearchLevel((UNIT_TYPES)unitType);
    }

    public void DecreaseResearchLevel(UNIT_TYPES unitType)
    {
        switch (unitType)
        {
            case UNIT_TYPES.SWORD:
                swordTier = Mathf.Max(0, swordTier - 1);
                break;
            case UNIT_TYPES.SPEAR:
                spearTier = Mathf.Max(0, spearTier - 1);
                break;
            case UNIT_TYPES.ARCHER:
                bowTier = Mathf.Max(0, bowTier - 1);
                break;
            case UNIT_TYPES.STONE:
                bowTier = Mathf.Max(0, bowTier - 1);
                break;
        }
    }

    public void IncreaseResearchLevel(int unitType)
    {
        IncreaseResearchLevel((UNIT_TYPES)unitType);
    }

    public void IncreaseResearchLevel(UNIT_TYPES unitType)
    {
        switch (unitType)
        {
            case UNIT_TYPES.SWORD:
                if(swordTier < maxSwordTier)
                {
                    swordTier++;
                }
                else if (swordTier == maxSwordTier)
                {
                    if (maxSwordTier < 3)
                    {
                        SpawnManager.ResourceCost cost = GetResearchCost(unitType);
                        if (GameController.Instance.resourcesManager.HaveEnoughResource(cost))
                        {
                            GameController.Instance.resourcesManager.ReduceResource(cost);
                            maxSwordTier++;
                            swordTier = maxSwordTier;
                        }
                    }
                }
                break;
            case UNIT_TYPES.SPEAR:
                if (spearTier < maxSpearTier)
                {
                    spearTier++;
                }
                else if (spearTier == maxSpearTier)
                {
                    if (maxSpearTier < 3)
                    {
                        SpawnManager.ResourceCost cost = GetResearchCost(unitType);
                        if (GameController.Instance.resourcesManager.HaveEnoughResource(cost))
                        {
                            GameController.Instance.resourcesManager.ReduceResource(cost);
                            maxSpearTier++;
                            spearTier = maxSpearTier;
                        }
                    }
                }
                break;
            case UNIT_TYPES.ARCHER:
                if (bowTier < maxBowTier)
                {
                    bowTier++;
                }
                else if (bowTier == maxBowTier)
                {
                    if (maxBowTier < 3)
                    {
                        SpawnManager.ResourceCost cost = GetResearchCost(unitType);
                        if (GameController.Instance.resourcesManager.HaveEnoughResource(cost))
                        {
                            GameController.Instance.resourcesManager.ReduceResource(cost);
                            maxBowTier++;
                            bowTier = maxBowTier;
                        }
                    }
                }
                break;
            case UNIT_TYPES.STONE:
                if (bowTier < maxBowTier)
                {
                    bowTier++;
                }
                else if (bowTier == maxBowTier)
                {
                    if (maxBowTier < 3)
                    {
                        SpawnManager.ResourceCost cost = GetResearchCost(unitType);
                        if (GameController.Instance.resourcesManager.HaveEnoughResource(cost))
                        {
                            GameController.Instance.resourcesManager.ReduceResource(cost);
                            maxBowTier++;
                            bowTier = maxBowTier;
                        }
                    }
                }
                break;
        }
    }

    public int GetTier(UNIT_TYPES unityType)
    {
        int ret = 0;

        switch (unityType)
        {
            case UNIT_TYPES.SWORD:
                ret = swordTier;
                break;
            case UNIT_TYPES.SPEAR:
                ret = spearTier;
                break;
            case UNIT_TYPES.ARCHER:
                ret = bowTier;
                break;
            case UNIT_TYPES.STONE:
                ret = bowTier;
                break;
        }

        return ret;
    }

    public int GetMaxTier(UNIT_TYPES unitType)
    {
        int ret = 0;

        switch (unitType)
        {
            case UNIT_TYPES.SWORD:
                ret = maxSwordTier;
                break;
            case UNIT_TYPES.SPEAR:
                ret = maxSpearTier;
                break;
            case UNIT_TYPES.ARCHER:
                ret = maxBowTier;
                break;
            case UNIT_TYPES.STONE:
                ret = maxBowTier;
                break;
        }

        return ret;
    }

    public void IncreaseResearchLevels()
    {
        swordTier = Mathf.Clamp(swordTier+1, 0, 3);
        spearTier = Mathf.Clamp(spearTier+1, 0, 3);
        bowTier = Mathf.Clamp(bowTier+1, 0, 3);
    }

    public void ReduceResearchLevels()
    {
        swordTier = Mathf.Clamp(swordTier-1, 0, 3);
        spearTier = Mathf.Clamp(spearTier-1, 0, 3);
        bowTier = Mathf.Clamp(bowTier-1, 0, 3);
    }
}
