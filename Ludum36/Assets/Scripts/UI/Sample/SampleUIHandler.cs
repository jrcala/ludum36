﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SampleUIHandler : MonoBehaviour {

    public ResourceBuildingHandler woodHandler;
    public ResourceBuildingHandler riceHandler;
    public ResourceBuildingHandler bronzeHandler;
    public ResourceBuildingHandler ironHandler;

    public ResourcesManager resourcesManager;

    public Text woodResourceTxt;
    public Text riceResourceTxt;
    public Text bronzeResourceTxt;
    public Text ironResourceTxt;

    public Text woodTxt;
    public Text riceTxt;
    public Text bronzeTxt;
    public Text ironTxt;

    // Update is called once per frame
    void Update () {
        woodResourceTxt.text = woodHandler.currentlyHeldResource.ToString("0") + " wood held";
        riceResourceTxt.text = riceHandler.currentlyHeldResource.ToString("0") + " rice held";
        bronzeResourceTxt.text = bronzeHandler.currentlyHeldResource.ToString("0") + " bronze held";
        ironResourceTxt.text = ironHandler.currentlyHeldResource.ToString("0") + " iron held";

        woodTxt.text = resourcesManager._wood.ToString("0") + " wood";
        riceTxt.text = resourcesManager._rice.ToString("0") + " rice";
        bronzeTxt.text = resourcesManager._bronze.ToString("0") + " bronze";
        ironTxt.text = resourcesManager._iron.ToString("0") + " iron";
    }
}
