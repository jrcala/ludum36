﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UpgradeUIHandler : MonoBehaviour {

    public Text woodResourceText;
    public Text riceResourceText;
    public Text bronzeResourceText;
    public Text ironResourceText;

    public Text swordCostText;
    public Text spearCostText;
    public Text bowCostText;

    public Image swordIcon;
    public Image spearIcon;
    public Image bowIcon;

    public Sprite[] swordIcons;
    public Sprite[] spearIcons;
    public Sprite[] bowIcons;

    private ResourcesManager resourceManager;
    private SpawnManager spawnManager;
    private ResearchManager researchManager;

    public void OnEnable()
    {
        resourceManager = GameController.Instance.resourcesManager;
        spawnManager = GameController.Instance.spawnManager;
        researchManager = GameController.Instance.researchManager;
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LateUpdate()
    {
        UpdateResourcesText();
        UpdateWeaponIcons();
        UpdateResearchCosts();
    }

    public void UpdateResearchCosts()
    {
        swordCostText.text = researchManager.GetCostString(UNIT_TYPES.SWORD);
        spearCostText.text = researchManager.GetCostString(UNIT_TYPES.SPEAR);
        bowCostText.text = researchManager.GetCostString(UNIT_TYPES.ARCHER);
    }

    public void UpdateWeaponIcons()
    {
        swordIcon.sprite = swordIcons[researchManager.GetTier(UNIT_TYPES.SWORD)];
        spearIcon.sprite = spearIcons[researchManager.GetTier(UNIT_TYPES.SPEAR)];
        bowIcon.sprite = bowIcons[researchManager.GetTier(UNIT_TYPES.ARCHER)];
    }

    public void UpdateResourcesText()
    {
        woodResourceText.text = resourceManager._wood.ToString("0");
        riceResourceText.text = resourceManager._rice.ToString("0");
        bronzeResourceText.text = resourceManager._bronze.ToString("0");
        ironResourceText.text = resourceManager._iron.ToString("0");
    }
}
