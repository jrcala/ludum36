﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BotPanelHandler : MonoBehaviour
{

    public Text woodResourceText;
    public Text riceResourceText;
    public Text bronzeResourceText;
    public Text ironResourceText;

    public Text swordsmanText;
    public Text spearmanText;
    public Text archerText;

    public Button swordsmanButton;
    public Button spearmanButton;
    public Button archerButton;

    private ResourcesManager resourceManager;
    private SpawnManager spawnManager;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        resourceManager = GameController.Instance.resourcesManager;
        spawnManager = GameController.Instance.spawnManager;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LateUpdate()
    {
        UpdateResourcesText();
        UpdateCostsText();
        CheckUnitButtonEnabled();
    }

    public void CheckUnitButtonEnabled()
    {
        swordsmanButton.interactable = spawnManager.CanSpawnUnit(UNIT_TYPES.SWORD);
        spearmanButton.interactable = spawnManager.CanSpawnUnit(UNIT_TYPES.SPEAR);
        archerButton.interactable = spawnManager.CanSpawnUnit(UNIT_TYPES.ARCHER);
    }

    public void UpdateResourcesText()
    {
        woodResourceText.text = resourceManager._wood.ToString("0");
        riceResourceText.text = resourceManager._rice.ToString("0");
        bronzeResourceText.text = resourceManager._bronze.ToString("0");
        ironResourceText.text = resourceManager._iron.ToString("0");
    }

    public void UpdateCostsText()
    {
        swordsmanText.text = SpawnManager.ResourceCostToString(
                                                  spawnManager.GetUnitCost(UNIT_TYPES.SWORD)
                                                                );
        spearmanText.text = SpawnManager.ResourceCostToString(
                                                  spawnManager.GetUnitCost(UNIT_TYPES.SPEAR)
                                                                );
        archerText.text = SpawnManager.ResourceCostToString(
                                                  spawnManager.GetUnitCost(UNIT_TYPES.ARCHER)
                                                                );
    }
}
