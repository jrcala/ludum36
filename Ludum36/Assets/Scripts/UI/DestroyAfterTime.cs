﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {

    public float time = 0.4f;


	// Use this for initialization
	void Start () {
        Invoke("DestroyMe", time);
        transform.position += new Vector3(
            Random.Range(-1, 10),
            Random.Range(-1,10),
            0
            );
	}


    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
