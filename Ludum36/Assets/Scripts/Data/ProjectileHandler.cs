﻿using UnityEngine;
using System.Collections;

public class ProjectileHandler : MonoBehaviour
{
    public float speed = 20f;
    public GameObject HitEffect;

    public Vector2 offset = Vector2.zero;
    public GameObject IntendedTarget;

    private Rigidbody2D objBody;

    public void Start() {
        Vector3 newPos = transform.position;
        newPos.x += offset.x;
        newPos.y += offset.y;

        transform.position = newPos;
    }

    public void OnEnable()
    {
        objBody = GetComponent<Rigidbody2D>();
        
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
        if (HitEffect != null && IntendedTarget == collision.collider.gameObject)
            Instantiate(HitEffect, collision.contacts[0].point, Quaternion.identity);
    }

    public void FixedUpdate()
    {
        Vector2 dir = Vector2.right;

        if (gameObject.layer == LayerMask.NameToLayer("Foe-Projectile"))
        {
            dir = Vector2.left;
            Vector3 projectileScale = transform.localScale;
            projectileScale.x *= -1;

            transform.localScale = projectileScale;
        }

        objBody.velocity = dir * speed;
    }
}
