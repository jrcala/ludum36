﻿using UnityEngine;
using System.Collections;
using System;

using UnityEngine.SceneManagement;

public class BaseHandler : MonoBehaviour, IDamageable {

    public int _hp = 500;
    public int def = 3;
    public UNIT_SIDE unitSide = UNIT_SIDE.ALLY;


    public void DestroyBuilding()
    {
        SceneManager.LoadScene(0);
    }

    #region IDamageable
    /// <summary>
    /// Damage this unit
    /// </summary>
    /// <param name="damage">Amount of damage desu</param>
    public void Damage(int damage)
    {
        _hp -= Mathf.Max((damage - def), 0);
        if (_hp <= 0)
        {
            DestroyBuilding();
        }
    }

    /// <summary>
    /// Is this ally or foe?
    /// </summary>
    /// <returns>Ally / Enemy?</returns>
    public UNIT_SIDE GetSide()
    {
        return unitSide;
    }
    #endregion
}
