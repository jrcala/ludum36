﻿using UnityEngine;
using System.Collections;

public class ResourceBuildingHandler : MonoBehaviour
{

    public RESOURCE_TYPES resourceManaged = RESOURCE_TYPES.WOOD;

    public float resourceEveryTick = 12;
    public float recieveResourceEvery = 5;
    public float currentlyHeldResource = 0;
    public float maxStoredResource = 1500;

    public ResourcesManager resourcesManager;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        if(resourcesManager == null)
        {
            resourcesManager = GameController.Instance.resourcesManager;
        }

        InvokeRepeating("ResourceTick", 1,1);
    }

    public void OnDisable()
    {
        StopCoroutine("ResourceTick");
    }

    void ResourceTick()
    {
        //currentlyHeldResource = Mathf.Clamp(currentlyHeldResource + resourceEveryTick / 5,0, maxStoredResource);
        resourcesManager.AddResource(resourceManaged, resourceEveryTick / recieveResourceEvery);
    }

    public void GetResource()
    {
        resourcesManager.AddResource(resourceManaged, currentlyHeldResource);
        currentlyHeldResource = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
