﻿using UnityEngine;
using System.Collections;

public class UnitUIHandler : MonoBehaviour
{
    public tk2dSprite unitSprite;

    public string movingSprite;
    public string attackingSprite;
    public string deadSprite;

    /// <summary>
    /// If the unit throws something- spawn this
    /// </summary>
    public GameObject projectile;

    public SpriteRenderer weaponHolder;
    public Sprite[] weapons;

    public Animator unitAnimator;
    public AudioSource audioSource;

    public int currentTier = 0;

    private UnitHandler unitHandler;
    private ResearchManager researchManager;

    // Use this for initialization
    void Start()
    {
        if (unitHandler.unitSide == UNIT_SIDE.FOE)
        {
            Vector3 unitScale = Vector3.one;
            unitScale.x = -1;
            transform.localScale = unitScale;
        }
        currentTier = unitHandler.currentTier;
    }

    public void OnEnable()
    {
        if (unitHandler == null)
        {
            unitHandler = GetComponent<UnitHandler>();
            researchManager = GameController.Instance.researchManager;
        }
        
        unitHandler.UnitAttack += UnitHandler_UnitAttack;
        unitHandler.UnitDead += UnitHandler_UnitDead;
    }

    public void OnDisable()
    {
        unitHandler.UnitAttack -= UnitHandler_UnitAttack;
    }

    private void UnitHandler_UnitAttack()
    {
        StartCoroutine(SetAttackSprite());
    }

    private void UnitHandler_UnitDead()
    {
        unitAnimator.SetTrigger("dead");
        unitSprite.SetSprite(unitSprite.GetSpriteIdByName(deadSprite));
        SendSpriteToBack();
    }

    private void SendSpriteToBack()
    {
        SpriteRenderer[] spriteRenderers = GetComponentsInChildren<SpriteRenderer>();

        for (int i = 0; i < spriteRenderers.Length; i++)
        {
            spriteRenderers[i].sortingOrder -= (int)(GameController.Instance.spawnManager.currentZ * 10);
        }
    }

    private IEnumerator SetAttackSprite()
    {
        //Set sprite to attacking sprite
        unitAnimator.SetTrigger("attack");
        unitSprite.SetSprite(unitSprite.GetSpriteIdByName(attackingSprite));

        //After a while...
        yield return new WaitForSeconds(unitHandler.attackSpeed / 2);
        if (unitHandler.unitState != UNIT_STATES.DEAD)
        {
            //Set it back to idle / moving xD
            unitSprite.SetSprite(unitSprite.GetSpriteIdByName(movingSprite));
        }
    }

    public void SpawnProjectile()
    {
        if (projectile != null)
        {
            GameObject spawnedProjectile = Instantiate<GameObject>(projectile);
            spawnedProjectile.transform.position = transform.position;
            spawnedProjectile.layer = LayerMask.NameToLayer(LayerMask.LayerToName(gameObject.layer) + "-Projectile");
            spawnedProjectile.GetComponent<ProjectileHandler>().IntendedTarget = unitHandler.currentTarget;
        }
    }

    public void LateUpdate()
    {
        weaponHolder.sprite = weapons[currentTier];
    }

    // Update is called once per frame
    void Update()
    {

    }
}
