﻿using UnityEngine;
using System.Collections;

public enum UNIT_STATES
{
    STANDBY,
    MOVING,
    ATTACKING,
    DEAD
}

public enum UNIT_SIDE
{
    ALLY,
    FOE
}

public enum UNIT_TYPES
{
    SWORD,
    STONE,
    ARCHER,
    SPEAR
}

public class UnitHandler : MonoBehaviour, IDamageable
{
    public UNIT_TYPES unitType = UNIT_TYPES.SWORD;
    public UNIT_STATES unitState = UNIT_STATES.STANDBY;
    public UNIT_SIDE unitSide = UNIT_SIDE.ALLY;

    public int _hp = 100;
    public float _range = 5;

    public int attack = 10;
    public int def = 2;
    public int speed = 10;
    public float attackSpeed = 0.5f;

    public int currentTier = 0;

    public GameObject currentTarget;

    private Rigidbody2D unitBody;
    private BoxCollider2D unitCollider;
    private Queue nextStates = new Queue();
    private bool isAttacking = false;

    public delegate void ChangedStateHandler(UNIT_STATES prevState, UNIT_STATES nextState);
    public event ChangedStateHandler ChangedState;

    public delegate void AttackHandler();
    public event AttackHandler UnitAttack;

    public delegate void DeadHandler();
    public event DeadHandler UnitDead;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        Initialize();
        ChangedState += UnitHandler_ChangedState;
        UnitAttack += UnitHandler_UnitAttack;
        UnitDead += UnitHandler_UnitDead;
    }

    public void OnDisable()
    {
        ChangedState -= UnitHandler_ChangedState;
        UnitAttack -= UnitHandler_UnitAttack;
        UnitDead -= UnitHandler_UnitDead;
    }

    private void UnitHandler_UnitDead()
    {
        Debug.Log("Unit just died");
        DisablePhysics();
    }

    public void EnablePhysics()
    {
        unitBody.isKinematic = false;
        unitCollider.enabled = true;
    }

    public void DisablePhysics()
    {
        unitBody.isKinematic = true;
        unitCollider.enabled = false; 
    }

    /// <summary>
    /// Get components and shit!
    /// </summary>
    void Initialize()
    {
        unitBody = GetComponent<Rigidbody2D>();
        unitCollider = GetComponent<BoxCollider2D>();
    }

    void Update()
    {
        if(nextStates.Count > 0)
        {
            ChangedState(unitState, (UNIT_STATES)nextStates.Dequeue());
        }
    }

    void FixedUpdate()
    {
        CheckStateAction();
    }

    #region ChangeState
    public void ChangeState(int nextState)
    {
        ChangeState((UNIT_STATES)nextState);
    }

    public void ChangeState(UNIT_STATES nextState)
    {
        nextStates.Enqueue(nextState);
    }

    /// <summary>
    /// Handles the changed state event by default
    /// </summary>
    /// <param name="prevState">Previous State</param>
    /// <param name="nextState">Next State</param>
    private void UnitHandler_ChangedState(UNIT_STATES prevState, UNIT_STATES nextState)
    {
        if(nextState == UNIT_STATES.ATTACKING)
        {
            //Check if unit  is already attacking so we dont send dupes
            if (!isAttacking)
            {
                InvokeRepeating("AttackCurrentTarget", 0, attackSpeed);
            }
        }

        if (prevState == UNIT_STATES.ATTACKING)
        {
            //if unit has stopped attacdking for some reason, stop sending attacking messages
            if (isAttacking)
            {
                StopCoroutine("AttackCurrentTarget");
                isAttacking = false;
            }
        }

        if (nextState == UNIT_STATES.DEAD)
        {
            UnitDead();
        }

        unitState = nextState;
    }
    #endregion

    public void AttackCurrentTarget()
    {
        isAttacking = true;

        if (unitState == UNIT_STATES.ATTACKING)
        {
            //Checks if there's still a target within range / switches target if ever nano
            if (GetEnemyUnitWithinRange(out currentTarget))
            {
                //Send event that unit is attacking~
                UnitAttack();
            }
            else
            {
                ChangeState(UNIT_STATES.MOVING);
            }
        }
        else
        {
            StopCoroutine("AttackCurrentTarget");
            isAttacking = false;
        }
    }

    private void UnitHandler_UnitAttack()
    {
        Debug.Log(gameObject.name + " is attacking " + currentTarget.name + " for " + attack);
        if(currentTarget.GetComponent<IDamageable>() != null)
        currentTarget.GetComponent<IDamageable>().Damage(attack);
    }

    /// <summary>
    /// One huge ass switch function to check what will the unit do
    /// </summary>
    void CheckStateAction()
    {
        if (unitState != UNIT_STATES.DEAD)
        {
            switch (unitState)
            {
                case UNIT_STATES.MOVING:
                    MoveUnit();
                    break;
            }
        }
    }

    /// <summary>
    /// Move unit on map based on speed
    /// </summary>
    void MoveUnit()
    {
        if (GetEnemyUnitWithinRange(out currentTarget))
        {
            ChangeState(UNIT_STATES.ATTACKING);
            unitBody.velocity = Vector2.zero;
        }
        else
        {
            //Move right by default
            Vector2 moveDir = Vector2.right * speed;

            //move left instead if enemy
            if(unitSide == UNIT_SIDE.FOE)
            {
                moveDir = Vector2.left * speed;
            }

            unitBody.velocity = moveDir;
        }
    }

    /// <summary>
    /// Checks if an enemy unit / structure is within range
    /// </summary>
    /// <param name="enemyUnit">enemy unit found- null if none</param>
    /// <returns>Did I find a target?</returns>
    bool GetEnemyUnitWithinRange(out GameObject enemyUnit)
    {
        //Check the right by default
        Vector2 raycastDir = Vector2.right;
        //Target enemyUnits by default
        int layerTarget = LayerMask.NameToLayer("Foe");

        //Check left and target player units if you are on enemy side
        if (unitSide == UNIT_SIDE.FOE)
        {
            raycastDir = Vector2.left;
            layerTarget = LayerMask.NameToLayer("Ally");
        }

        //Will make this raycast only the target layer
        int layerMask = 1 << layerTarget;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, raycastDir, _range, layerMask);

        Debug.DrawRay(transform.position, raycastDir * _range, Color.red);

        //If target is within range desu
        if (hit.collider != null)
        {
            //Out the nearest enemy unit hit;
            enemyUnit = hit.collider.gameObject;
            //We hit shit! :))
            return true;
        }
        else
        {
            enemyUnit = null;
            //Out of range
            return false;
        }
    }

    #region IDamageable
    /// <summary>
    /// Damage this unit
    /// </summary>
    /// <param name="damage">Amount of damage desu</param>
    public void Damage(int damage)
    {
        _hp -= Mathf.Max((damage - def), 0);
        if (_hp <= 0)
        {
            ChangeState(UNIT_STATES.DEAD);
        }
    }

    /// <summary>
    /// Is this ally or foe?
    /// </summary>
    /// <returns>Ally / Enemy?</returns>
    public UNIT_SIDE GetSide()
    {
        return unitSide;
    }
    #endregion
}
