﻿using UnityEngine;
using System.Collections;

public class UnitSoundHandler : MonoBehaviour
{

    private AudioSource audioSource;
    private UnitHandler unitHandler;
    private UnitUIHandler unitUIHandler;
    private SoundHandler soundHandler;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        audioSource = GetComponentInParent<AudioSource>();
        unitHandler = GetComponentInParent<UnitHandler>();
        unitUIHandler = GetComponentInParent<UnitUIHandler>();
        soundHandler = GameController.Instance.soundHandler;
    }

    public void PlayAttackSound()
    {
        AudioClip clipToPlay = null;

        switch (unitHandler.unitType)
        {
            case UNIT_TYPES.SWORD:
                clipToPlay = soundHandler.GetSwordSFX();
                break;
            case UNIT_TYPES.ARCHER:
                clipToPlay = soundHandler.GetArcherSFX();
                break;
            case UNIT_TYPES.SPEAR:
                clipToPlay = soundHandler.GetSpearSFX();
                break;
            case UNIT_TYPES.STONE:
                clipToPlay = soundHandler.GetStoneSFX();
                break;
        }

        unitUIHandler.SpawnProjectile();
        PlaySound(clipToPlay);
    }

    private void PlaySound(AudioClip clipToPlay)
    {
        if (clipToPlay != null) audioSource.PlayOneShot(clipToPlay);
    }

}
