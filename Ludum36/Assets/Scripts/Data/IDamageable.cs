﻿
/// <summary>
/// Stuff with this can me damaged~
/// </summary>
public interface IDamageable {

    void Damage(int damage);
    UNIT_SIDE GetSide();

}
