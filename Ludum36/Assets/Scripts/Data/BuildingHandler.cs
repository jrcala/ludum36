﻿using UnityEngine;
using System.Collections;
using System;

public class BuildingHandler : MonoBehaviour, IDamageable {

    public UNIT_SIDE unitSide = UNIT_SIDE.ALLY;
    public int _hp = 100;
    
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    #region IDamageable
    public void Damage(int damage)
    {
        _hp -= damage;
    }

    public UNIT_SIDE GetSide()
    {
        return unitSide;
    }
    #endregion
}
