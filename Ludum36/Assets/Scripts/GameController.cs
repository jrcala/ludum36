﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    public ResourcesManager resourcesManager;
    public SpawnManager spawnManager;
    public SoundHandler soundHandler;
    public ResearchManager researchManager;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        if(GameController.Instance == null)
        {
            GameController.Instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
