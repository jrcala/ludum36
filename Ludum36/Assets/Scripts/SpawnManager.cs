﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnManager : MonoBehaviour
{
    [System.Serializable]
    public struct ResourceCost
    {
        public float rice;
        public float wood;
        public float bronze;
        public float iron;
    }

    [System.Serializable]
    public struct UnitCost
    {
        public ResourceCost[] resourceCost;
    }

    public GameObject[] UnitTypes;
    public Transform[] SpawnPoints;
    public UnitCost[] unitCosts;

    public ResourcesManager resourcesManager;
    public ResearchManager researchManager;

    //current Layer offset
    public float currentZ = 0;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        GameController.Instance.spawnManager = this;
        resourcesManager = GameController.Instance.resourcesManager;
        researchManager = GameController.Instance.researchManager;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnAllyUnit(int unitType)
    {
        SpawnAllyUnit((UNIT_TYPES)unitType);
    }

    public void SpawnAllyUnit(UNIT_TYPES unitType)
    {
        SpawnUnit(unitType, UNIT_SIDE.ALLY);
    }

    public void SpawnFoeUnit(int unitType)
    {
        SpawnFoeUnit((UNIT_TYPES)unitType);
    }

    public void SpawnFoeUnit(UNIT_TYPES unitType)
    {
        SpawnUnit(unitType, UNIT_SIDE.FOE);
    }

    public void SpawnUnit(UNIT_TYPES unitType, UNIT_SIDE unitSide, int tier = -1)
    {
        if (tier == -1) tier = researchManager.GetTier(unitType);

        if (resourcesManager.HaveEnoughResource(unitCosts[(int)unitType].resourceCost[tier]) || unitSide == UNIT_SIDE.FOE)
        {
            if (unitType == UNIT_TYPES.ARCHER && tier == 0) unitType = UNIT_TYPES.STONE;

            GameObject unitSpawned = Instantiate<GameObject>(UnitTypes[(int)unitType]);
            unitSpawned.transform.position = SpawnPoints[(int)unitSide].position;
            unitSpawned.GetComponent<UnitHandler>().currentTier = tier;
            currentZ++;
            Vector3 pos = unitSpawned.transform.position;

            SpriteRenderer[] spriteRenderers = unitSpawned.GetComponentsInChildren<SpriteRenderer>();

            for (int i = 0; i < spriteRenderers.Length; i++)
            {
                spriteRenderers[i].sortingOrder += (int)(currentZ * 10);
            }

            unitSpawned.GetComponent<UnitHandler>().unitSide = unitSide;
            if (unitSide == UNIT_SIDE.ALLY)
            {
                unitSpawned.layer = LayerMask.NameToLayer("Ally");
            }
            else if (unitSide == UNIT_SIDE.FOE)
            {
                unitSpawned.layer = LayerMask.NameToLayer("Foe");
            }

            unitSpawned.GetComponent<UnitHandler>().ChangeState(UNIT_STATES.MOVING);

            if (unitSide == UNIT_SIDE.ALLY) resourcesManager.ReduceResource(unitCosts[(int)unitType].resourceCost[tier]);
        }
    }

    public bool CanSpawnUnit(UNIT_TYPES unitType, int tier = -1)
    {
        if(tier == -1) tier = researchManager.GetTier(unitType);

        return resourcesManager.HaveEnoughResource(unitCosts[(int)unitType].resourceCost[tier]);
    }

    public ResourceCost GetUnitCost(UNIT_TYPES unitType, int tier = -1)
    {
        ResourceCost ret = new ResourceCost();
        if (tier == -1) tier = researchManager.GetTier(unitType);

        ret = unitCosts[(int)unitType].resourceCost[tier];

        return ret;
    }

    public static string ResourceCostToString(ResourceCost resource)
    {
        string ret = "";

        ret = resource.rice + " / " + resource.wood + " / " + resource.bronze + " / " + resource.iron;

        return ret;
    }
}
