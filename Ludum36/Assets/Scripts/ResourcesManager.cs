﻿using UnityEngine;
using System.Collections;

public enum RESOURCE_TYPES
{
    RICE,
    WOOD,
    BRONZE,
    IRON
}

public class ResourcesManager : MonoBehaviour
{

    public float _rice = 0;
    public float _wood = 0;
    public float _bronze = 0;
    public float _iron = 0;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        GameController.Instance.resourcesManager = this;
    }

    public void ReduceResource(SpawnManager.ResourceCost cost)
    {
        _rice -= cost.rice;
        _wood -= cost.wood;
        _bronze -= cost.bronze;
        _iron -= cost.iron;
    }

    public bool HaveEnoughResource(SpawnManager.ResourceCost costToCheck)
    {
        return _rice >= costToCheck.rice &&
            _wood >= costToCheck.wood &&
            _bronze >= costToCheck.bronze &&
            _iron >= costToCheck.iron;
    }

    public void AddResource(RESOURCE_TYPES resourceToAdd, float value)
    {
        switch (resourceToAdd)
        {
            case RESOURCE_TYPES.WOOD:
                _wood += value;
                break;
            case RESOURCE_TYPES.RICE:
                _rice += value;
                break;
            case RESOURCE_TYPES.BRONZE:
                _bronze += value;
                break;
            case RESOURCE_TYPES.IRON:
                _iron += value;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
