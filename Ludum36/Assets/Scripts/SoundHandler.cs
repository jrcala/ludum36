﻿using UnityEngine;
using System.Collections;

public class SoundHandler : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip BGM;

    public AudioClip[] SwordsSFX;
    public AudioClip[] SpearsSFX;
    public AudioClip[] StoneSFX;
    public AudioClip[] ArcherSFX;

    // Use this for initialization
    void Start()
    {

    }

    public void OnEnable()
    {
        if(audioSource == null)
        {
            audioSource = GetComponent<AudioSource>();
            GameController.Instance.soundHandler = this;
        }

        if (!audioSource.isPlaying)
        {
            audioSource.loop = true;
            audioSource.clip = BGM;
            audioSource.Play();
        }
    }

    public void OnDisable()
    {

    }

    public AudioClip GetSwordSFX()
    {
        return GetRandomSFXFrom(SwordsSFX);
    }

    public AudioClip GetSpearSFX()
    {
        return GetRandomSFXFrom(SpearsSFX);
    }

    public AudioClip GetStoneSFX()
    {
        return GetRandomSFXFrom(StoneSFX);
    }

    public AudioClip GetArcherSFX()
    {
        return GetRandomSFXFrom(ArcherSFX);
    }

    public AudioClip GetRandomSFXFrom(AudioClip[] sfxList)
    {

        if (sfxList.Length == 0) return null;

        return sfxList[Random.Range(0, sfxList.Length)];
    }

    // Update is called once per frame
    void Update()
    {

    }
}
